// Fill out your copyright notice in the Description page of Project Settings.


#include "FoodElementBonus.h"

// Sets default values
AFoodElementBonus::AFoodElementBonus()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFoodElementBonus::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFoodElementBonus::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

